*Deutsch*
# Outline Autogroup
Eine simple Express.JS API die Requests vom Wiki entgegennimmt.

## Setup
- Installiere die neuste Version von NodeJS
- Clone die Repo
- Kopiere die `config.example.json` in `config.json` und fülle sie aus
- `npm install`
- `npm start` und die API läuft und empfängt nun Requests von Outline

*English*
# Outline Autogroup
A simple ExpressJS API that receives requests from Outline.

## Setup
- Install the latest version of NodeJS
- Clone the Repository
- Copy `config.example.json` to `config.json` and fill in your data
- run `npm install`
- run `npm start` and the API runs and now receives requests from Outline