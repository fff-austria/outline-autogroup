import express from 'express';
import axios from 'axios';
import { Hmac, createHmac } from 'node:crypto';
// eslint-disable-next-line @typescript-eslint/no-var-requires
const config = require('./config.json');

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.listen(config.port, () => {
	console.log(`API listening on port ${config.port}`);
});

app.post(config.path, async (req, res) => {
	console.log(req.body, req.headers);

	// check if signature is valid
	const signatureValid: boolean = await checkSignature(config.secret, req.headers['outline-signature']);
	if (!signatureValid) {
		res.status(403).send({ error: 'The signature check failed!' });
	}

	const data = {
		'id': config.group_id,
		'userId': req.body.payload.id
	};
	const response = await axios.post(config.instance + '/api/groups.add_user', data);

	console.log(response);

}).catch((error: object) => {
	console.log('Error: ' + error);
});

const checkSignature = async (secret: string, signature_header: string) => {
	// split the signature header in timestamp and signature parts.
	const timestamp: string = await signature_header.split(',')[0].split('=')[1];
	const signature: string = await signature_header.split(',')[1].split('=')[1];

	// generate hash with the signing secret
	const hmac: Hmac = createHmac('sha256', secret);
	const hash: string = hmac.update(`t=${timestamp}`).digest('hex');

	// check if signature equals the newly generated hash
	if (hash === signature) {
		return true;
	} else {
		return false;
	}
};